package com.griddynamics.httpserver.container;

import com.griddynamics.httpserver.HandlerContainer;
import com.griddynamics.httpserver.api.RequestHandler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class HandlerContainerImpl implements HandlerContainer {

    private static final Logger LOGGER = Logger.getLogger(HandlerContainerImpl.class.getName());

    private final Map<String, RequestHandler> requestHandlerContainer = new ConcurrentHashMap<>();

    private static final String DEFAULT_HANDLER = "/***";

    private static HandlerContainerImpl instance;

    private HandlerContainerImpl() {
    }

    public static synchronized HandlerContainerImpl getInstance() {
        if (instance == null) {
            synchronized (HandlerContainerImpl.class) {
                if (instance == null) {
                    instance = new HandlerContainerImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public void putRequestHandler(RequestHandler requestHandler) {
        String requestMapping = requestHandler.requestMapping();
        LOGGER.info("Put request handler with request mapping: " + requestMapping);
        requestHandlerContainer.put(requestMapping, requestHandler);
    }

    @Override
    public RequestHandler getRequestHandler(String requestMapping) {
        LOGGER.info("Get request handler with request mapping: " + requestMapping);
        RequestHandler requestHandler = requestHandlerContainer.get(requestMapping);
        if (requestHandler == null) {
            LOGGER.info("Request handler not found, get default");
            return requestHandlerContainer.get(DEFAULT_HANDLER);
        }
        return requestHandlerContainer.get(requestMapping);
    }

}
