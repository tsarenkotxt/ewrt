package com.griddynamics.httpserver.definition;

import com.griddynamics.httpserver.HandlerContainer;
import com.griddynamics.httpserver.HandlerDefinition;
import com.griddynamics.httpserver.api.RequestHandler;
import com.griddynamics.httpserver.util.Utils;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class HandlerDefinitionImpl implements HandlerDefinition {

    private static final Logger LOGGER = Logger.getLogger(HandlerDefinitionImpl.class.getName());


    private final String[] handlerPackages;

    public HandlerDefinitionImpl(String[] handlerPackages) {
        this.handlerPackages = handlerPackages;
    }

    @Override
    public void defineHandlers(HandlerContainer handlerContainer) {
        List<Class> classes;
        if (handlerPackages != null) {
            classes = Utils.getClasses(Arrays.asList(handlerPackages));
        } else {
            List<String> packages = Utils.getPackages();
            classes = Utils.getClasses(packages);
        }
        List<RequestHandler> handlers = getHandlers(classes);
        handlers.forEach(handlerContainer::putRequestHandler);
        LOGGER.info(String.format("Define %s request handlers", handlers.size()));
    }

    @Override
    public List<RequestHandler> getHandlers(List<Class> classes) {
        return classes.stream()
                .distinct()
                .filter(clazz -> clazz.getSuperclass() != null)
                .filter(clazz -> clazz.getSuperclass().equals(RequestHandler.class))
                .map(clazz -> {
                    RequestHandler requestHandler = null;
                    try {
                        requestHandler = (RequestHandler) clazz.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return requestHandler;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }


}
