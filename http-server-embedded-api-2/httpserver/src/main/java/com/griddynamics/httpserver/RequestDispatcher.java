package com.griddynamics.httpserver;

import com.griddynamics.httpserver.api.Request;
import com.griddynamics.httpserver.api.RequestHandler;
import com.griddynamics.httpserver.api.Response;

import java.io.OutputStream;

public interface RequestDispatcher {

    void doDispatch(Request request, OutputStream output) throws Exception;

    void dispatchHandler(RequestHandler requestHandler, Request request,
                         Response response) throws Exception;

}
