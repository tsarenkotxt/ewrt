package com.griddynamics.httpserver;

import com.griddynamics.httpserver.api.RequestHandler;

import java.util.List;

public interface HandlerDefinition {

    void defineHandlers(HandlerContainer handlerContainer);

    List<RequestHandler> getHandlers(List<Class> classes);

}
