package com.griddynamics.httpserver.handler;

import com.griddynamics.httpserver.RootHandler;

import java.io.InputStream;
import java.io.OutputStream;

public abstract class AbstractRootHandlerFactory {

    public abstract RootHandler createRootHandler(InputStream input, OutputStream output);

}
