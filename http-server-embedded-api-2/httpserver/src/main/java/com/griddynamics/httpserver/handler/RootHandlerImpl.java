package com.griddynamics.httpserver.handler;

import com.griddynamics.httpserver.RequestDispatcher;
import com.griddynamics.httpserver.api.Request;
import com.griddynamics.httpserver.parser.RequestParser;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

public class RootHandlerImpl implements com.griddynamics.httpserver.RootHandler {

    private static final Logger LOGGER = Logger.getLogger(RootHandlerImpl.class.getName());

    private final RequestDispatcher requestDispatcher;
    private final InputStream input;
    private final OutputStream output;

    public RootHandlerImpl(RequestDispatcher requestDispatcher,
                           InputStream input, OutputStream output) {
        this.requestDispatcher = requestDispatcher;
        this.input = input;
        this.output = output;
    }

    @Override
    public void run() {
        try {
            RequestParser parser = new RequestParser();
            Request request = parser.parseRequest(input);
            requestDispatcher.doDispatch(request, output);
        } catch (Exception e) {
            // response 500
            LOGGER.info(e.getLocalizedMessage());
        }
    }

}
