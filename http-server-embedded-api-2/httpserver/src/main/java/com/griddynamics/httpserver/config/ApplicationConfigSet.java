package com.griddynamics.httpserver.config;

public class ApplicationConfigSet {

    public static final int PORT = 8080;
    public static final int THREAD_POOL = 150;

}
