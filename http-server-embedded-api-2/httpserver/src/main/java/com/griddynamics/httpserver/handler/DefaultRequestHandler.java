package com.griddynamics.httpserver.handler;

import com.griddynamics.httpserver.api.RequestHandler;

public class DefaultRequestHandler extends RequestHandler {

    @Override
    public String requestMapping() {
        return "/***";
    }

}
