package com.griddynamics.httpserver.manager;

import com.griddynamics.httpserver.RootHandler;
import com.griddynamics.httpserver.SocketManager;
import com.griddynamics.httpserver.exception.ListenSocketException;
import com.griddynamics.httpserver.exception.OpenSocketException;
import com.griddynamics.httpserver.handler.AbstractRootHandlerFactory;
import com.griddynamics.httpserver.handler.RootHandlerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class SocketManagerImpl implements SocketManager {

    private static final Logger LOGGER = Logger.getLogger(SocketManagerImpl.class.getName());

    private int port;

    private final AbstractRootHandlerFactory rootHandlerFactory = new RootHandlerFactory();
    private ExecutorService executorService;
    private ServerSocket serverSocket;


    public SocketManagerImpl(int port, int threadPool) {
        this.port = port;
        this.executorService = Executors.newFixedThreadPool(threadPool);
    }

    @Override
    public void openSocket() {
        try {
            serverSocket = new ServerSocket(port);
            LOGGER.info("Server started on port: " + serverSocket.getLocalPort());
        } catch (IOException e) {
            throw new OpenSocketException(String.format("Port %s is blocked", port), e);
        }
        listenSocket(serverSocket);
    }

    @Override
    public void listenSocket(ServerSocket serverSocket) {
        LOGGER.info("Listen server socket");
        while (!serverSocket.isClosed() && !executorService.isShutdown()) {
            try {
                Socket socket = serverSocket.accept();
                RootHandler rootHandler = rootHandlerFactory
                        .createRootHandler(socket.getInputStream(), socket.getOutputStream());
                executorService.submit(rootHandler);
            } catch (IOException e) {
                throw new ListenSocketException("Failed to establish connection", e);
            }
        }
    }

    @Override
    public void closeSocket() {
        LOGGER.info("Closing server socket");
        executorService.shutdownNow();
        try {
            serverSocket.close();
        } catch (IOException e) {
            LOGGER.info("Failed to close server socket");
        }
        LOGGER.info("Server socket is closed");
    }

}
