package com.griddynamics.httpserver;

import com.griddynamics.httpserver.container.HandlerContainerImpl;
import com.griddynamics.httpserver.definition.HandlerDefinitionImpl;
import com.griddynamics.httpserver.manager.SocketManagerImpl;

import static com.griddynamics.httpserver.config.ApplicationConfigSet.PORT;
import static com.griddynamics.httpserver.config.ApplicationConfigSet.THREAD_POOL;

public class HttpServer {

    private final HandlerContainer handlerContainer;
    private final HandlerDefinition handlerDefinition;
    private final SocketManager socketManager;

    private HttpServer(HandlerContainer handlerContainer,
                       HandlerDefinition handlerDefinition,
                       SocketManager server) {
        this.handlerContainer = handlerContainer;
        this.handlerDefinition = handlerDefinition;
        this.socketManager = server;
    }

    public void run() {
        handlerDefinition.defineHandlers(handlerContainer);
        this.socketManager.openSocket();
    }

    public void shutdown() {
        this.socketManager.closeSocket();
    }

    public static class Builder {

        private int port = PORT;
        private int threadPool = THREAD_POOL;
        private String[] handlerPackages = null;

        private HandlerContainer handlerContainer = HandlerContainerImpl.getInstance();
        private HandlerDefinition handlerDefinition;
        private SocketManager socketManager;

        public Builder() {
        }

        public Builder setHandlerContainer(com.griddynamics.httpserver.HandlerContainer handlerContainer) {
            this.handlerContainer = handlerContainer;
            return this;
        }

        public Builder setHandlerDefinition(com.griddynamics.httpserver.HandlerDefinition handlerDefinition) {
            this.handlerDefinition = handlerDefinition;
            return this;
        }

        public Builder setSocketManager(SocketManager socketManager) {
            this.socketManager = socketManager;
            return this;
        }

        public Builder setPort(int port) {
            this.port = port;
            return this;
        }

        public Builder setThreadPool(int threadPool) {
            this.threadPool = threadPool;
            return this;
        }

        public Builder setHandlerPackages(String[] handlerPackages) {
            this.handlerPackages = handlerPackages;
            return this;
        }

        public HttpServer build() {
            socketManager = new SocketManagerImpl(port, threadPool);
            handlerDefinition = new HandlerDefinitionImpl(handlerPackages);
            return new HttpServer(handlerContainer, handlerDefinition, socketManager);
        }

    }

}
