package com.griddynamics.httpserver;

import com.griddynamics.httpserver.api.RequestHandler;

public interface HandlerContainer {

    void putRequestHandler(RequestHandler requestHandler);

    RequestHandler getRequestHandler(String requestMapping);

}
