package com.griddynamics.httpserver.exception;

public class ListenSocketException extends RuntimeException {

    public ListenSocketException(String message) {
        super(message);
    }

    public ListenSocketException(String message, Throwable cause) {
        super(message, cause);
    }

}
