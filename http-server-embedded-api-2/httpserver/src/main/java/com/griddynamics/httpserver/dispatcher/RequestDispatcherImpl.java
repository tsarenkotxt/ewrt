package com.griddynamics.httpserver.dispatcher;

import com.griddynamics.httpserver.HandlerContainer;
import com.griddynamics.httpserver.RequestDispatcher;
import com.griddynamics.httpserver.api.Request;
import com.griddynamics.httpserver.api.RequestHandler;
import com.griddynamics.httpserver.api.Response;

import java.io.OutputStream;
import java.util.logging.Logger;

public class RequestDispatcherImpl implements RequestDispatcher {

    private static final Logger LOGGER = Logger.getLogger(RequestDispatcherImpl.class.getName());

    private final HandlerContainer handlerContainer;

    public RequestDispatcherImpl(HandlerContainer handlerContainer) {
        this.handlerContainer = handlerContainer;
    }

    @Override
    public void doDispatch(Request request, OutputStream output) throws Exception {
        String uri = request.getUri();
        RequestHandler requestHandler = handlerContainer.getRequestHandler(uri);
        LOGGER.info("Dispatch request handler with request mapping: " +
                requestHandler.requestMapping());
        Response response = new Response();
        response.setOutputStream(output);
        dispatchHandler(requestHandler, request, response);
    }

    public void dispatchHandler(RequestHandler requestHandler, Request request,
                                Response response) throws Exception {
        String method = request.getMethod();
        switch (method) {
            case "POST": {
                requestHandler.doPost(request, response);
                break;
            }
            case "PUT": {
                requestHandler.doPut(request, response);
                break;
            }
            case "DELETE": {
                requestHandler.doDelete(request, response);
                break;
            }
            default: {
                requestHandler.doGet(request, response);
            }
        }
    }

}
