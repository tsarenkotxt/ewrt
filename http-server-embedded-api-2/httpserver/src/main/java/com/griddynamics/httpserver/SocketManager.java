package com.griddynamics.httpserver;

import java.net.ServerSocket;

public interface SocketManager {

    void openSocket();

    void listenSocket(ServerSocket serverSocket);

    void closeSocket();

}
