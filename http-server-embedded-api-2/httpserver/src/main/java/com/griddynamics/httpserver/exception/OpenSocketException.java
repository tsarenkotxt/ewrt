package com.griddynamics.httpserver.exception;

public class OpenSocketException extends RuntimeException {

    public OpenSocketException(String message) {
        super(message);
    }

    public OpenSocketException(String message, Throwable cause) {
        super(message, cause);
    }

}
