package com.griddynamics.httpserver.parser;

import com.griddynamics.httpserver.io.HttpInputStream;
import com.griddynamics.httpserver.api.Request;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static com.griddynamics.httpserver.util.Utils.*;

public class RequestParser {

    private final static String LINE_DELIMITER = "\r\n";
    private final static String ABSOLUTE_URI_DELIMITER = " ";
    private final static Integer ABSOLUTE_URI_INDEX = 0;
    private final static Integer URI_INDEX = 1;
    private final static String PARAMETERS_START_SYMBOL = "?";
    private final static String PARAMETERS_DELIMITER = "&";
    private final static Integer PARAMETERS_ARRAY_KEY_INDEX = 0;
    private final static Integer PARAMETERS_ARRAY_VALUE_INDEX = 1;
    private final static String PARAMETERS_KEY_VALUE_DELIMITER = "=";
    private final static String HEADER_REGEX = "^[a-zA-Z-]+[:\\s].*";
    private final static String HEADER_LINE_DELIMITER = ": ";
    private final static Integer HEADER_START_INDEX = 1;
    private final static Integer HEADER_MAP_KEY_INDEX = 0;
    private final static Integer METHOD_INDEX = 0;

    public Request parseRequest(InputStream input) throws Exception {
        HttpInputStream httpInput = new HttpInputStream(input);
        byte[] headerBytes = httpInput.readHeader();
        String header = new String(headerBytes, 0, headerBytes.length);
        String[] headerLines = header.split(LINE_DELIMITER);
        String method = getPartOfString(headerLines[ABSOLUTE_URI_INDEX],
                ABSOLUTE_URI_DELIMITER, METHOD_INDEX);
        String uriWithParameters = getPartOfString(headerLines[ABSOLUTE_URI_INDEX],
                ABSOLUTE_URI_DELIMITER, URI_INDEX);
        String uri = parseUri(uriWithParameters);
        Map<String, String> headers = parseHeaders(headerLines);
        Request request = new Request();
        request.setUri(uri);
        request.setMethod(method);
        request.setHeader(headers);
        request.setParameters(parseUriParameters(uriWithParameters));
        request.setBodyInputStream(input);
        return request;
    }

    private Map<String, String> parseHeaders(String[] requestLines) {
        Map<String, String> headers = new HashMap<>();
        for (int i = HEADER_START_INDEX; i < requestLines.length; i++) {
            boolean isHeader = isFindByRegEx(requestLines[i], HEADER_REGEX);
            if (!isHeader) {
                break;
            }
            Map<String, String> header = stringToMap(requestLines[i],
                    HEADER_LINE_DELIMITER, HEADER_MAP_KEY_INDEX);
            headers.putAll(header);
        }
        return headers;
    }

    private String parseUri(String uri) {
        if (!uri.contains(PARAMETERS_START_SYMBOL)) {
            return uri;
        }
        return getPartOfString(uri, Pattern.quote(PARAMETERS_START_SYMBOL),
                ABSOLUTE_URI_INDEX);
    }

    private Map<String, String> parseUriParameters(String uri) {
        int startIndex = uri.indexOf(PARAMETERS_START_SYMBOL) + 1;
        String uriParametersLine = uri.substring(startIndex);
        String[] parameters = uriParametersLine.split(PARAMETERS_DELIMITER);
        return stringToMap(parameters, PARAMETERS_KEY_VALUE_DELIMITER,
                PARAMETERS_ARRAY_KEY_INDEX, PARAMETERS_ARRAY_VALUE_INDEX);
    }

}
