package com.griddynamics.httpserver.handler;

import com.griddynamics.httpserver.container.HandlerContainerImpl;
import com.griddynamics.httpserver.dispatcher.RequestDispatcherImpl;

import java.io.InputStream;
import java.io.OutputStream;

public class RootHandlerFactory extends AbstractRootHandlerFactory {

    @Override
    public RootHandlerImpl createRootHandler(InputStream input, OutputStream output) {
        HandlerContainerImpl handlerContainer = HandlerContainerImpl.getInstance();
        RequestDispatcherImpl requestDispatcher = new RequestDispatcherImpl(handlerContainer);
        return new RootHandlerImpl(requestDispatcher, input, output);
    }

}
