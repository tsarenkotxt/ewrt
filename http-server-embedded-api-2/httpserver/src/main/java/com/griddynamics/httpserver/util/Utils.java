package com.griddynamics.httpserver.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class Utils {

    private Utils() {
    }


    public static String getPartOfString(String line, String delimiter, int index) {
        String[] split = line.split(delimiter);
        return split[index];
    }

    public static boolean isFindByRegEx(String line, String regEx) {
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(line);
        return matcher.find();
    }

    public static Map<String, String> stringToMap(String line, String delimiter, int keyIndex) {
        String[] split = line.split(delimiter);
        Map<String, String> stringMap = new HashMap<>();
        String key = split[keyIndex];
        String value = line.replace(key + delimiter, "");
        stringMap.put(key, value);
        return stringMap;
    }

    public static Map<String, String> stringToMap(String[] lines, String delimiter,
                                                  int keyIndex, int valueIndex) {
        Map<String, String> parameterMap = new HashMap<>();
        for (String param : lines) {
            String[] paramKeyValue = param.split(delimiter);
            if (paramKeyValue.length < 2) {
                break;
            }
            parameterMap.put(paramKeyValue[keyIndex],
                    paramKeyValue[valueIndex]);
        }
        return parameterMap;
    }

    public static List<String> getPackages() {
        return Arrays.stream(Package.getPackages())
                .filter(p ->
                        !p.getName().startsWith("java.") &&
                                !p.getName().startsWith("sun.") &&
                                !p.getName().startsWith("jdk.")
                )
                .map(Package::getName)
                .collect(Collectors.toList());
    }

    public static List<Class> getClasses(List<String> packages) {
        return packages.stream().map(p -> {
            try {
                return getClasses(p);
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
            return null;
        })
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public static List<Class> getClasses(String packageName)
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes;
    }


    public static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

}
