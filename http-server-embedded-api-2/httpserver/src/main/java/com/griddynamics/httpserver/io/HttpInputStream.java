package com.griddynamics.httpserver.io;

import java.io.IOException;
import java.io.InputStream;

public class HttpInputStream {

    private static final byte[] END_OF_LINE = {13, 10, 13, 10};
    private static final int BUFFER_SIZE = 1024;
    private static final int MAX_BUFFER_SIZE = 8192;
    private static final double LOAD_FACTORY = 0.5;

    private final InputStream inputStream;

    public HttpInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public byte[] readHeader() throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        boolean isEOL = false;
        int headerLength = 0;
        for (int i = 0; i < buffer.length; i++) {
            if (isEOL) {
                break;
            }
            byte read = (byte) inputStream.read();
            if (read == -1) {
                break;
            } else {
                buffer[i] = read;
                if (isFilledBuffer(i, buffer)) {
                    buffer = increaseBuffer(buffer, LOAD_FACTORY);
                }
            }
            if (i > END_OF_LINE.length) {
                isEOL = findEndOfLine(buffer, i);
            }
            headerLength = i + 1;
        }
        byte[] header = new byte[headerLength];
        System.arraycopy(buffer, 0, header, 0, headerLength);
        return header;
    }

    private boolean findEndOfLine(byte[] src, int indexSrc) {
        boolean found = false;
        for (int i = indexSrc, j = END_OF_LINE.length - 1; j >= 0; i--, j--) {
            found = src[i] == END_OF_LINE[j];
            if (!found) {
                break;
            }
        }
        return found;
    }

    private boolean isFilledBuffer(int index, byte[] buffer) {
        return index == (buffer.length - 1) &&
                (buffer.length * LOAD_FACTORY) <= MAX_BUFFER_SIZE;
    }

    private byte[] increaseBuffer(byte[] filledArray, double loadFactory) {
        int newArraySize = (int) (filledArray.length + (filledArray.length * loadFactory));
        byte[] newArray = new byte[newArraySize];
        System.arraycopy(filledArray, 0, newArray, 0, filledArray.length);
        return newArray;
    }

}
