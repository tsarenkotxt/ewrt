package com.griddynamics.httpserver.example;

import com.griddynamics.httpserver.api.HttpOutputStream;
import com.griddynamics.httpserver.api.Request;
import com.griddynamics.httpserver.api.RequestHandler;
import com.griddynamics.httpserver.api.Response;

import java.io.File;
import java.nio.file.Files;

public class SimpleRequestHandler extends RequestHandler {

    @Override
    public String requestMapping() {
        return "/example";
    }

    @Override
    public void doGet(Request request, Response response) throws Exception {
        File html = new File(SimpleRequestHandler.class.getClassLoader().
                getResource("index.html").getFile());
        byte[] responseBody = Files.readAllBytes(html.toPath());
        response.setContentType("text/html; charset=UTF-8");
        response.setStatusCode(200);
        HttpOutputStream httpOutputStream = new HttpOutputStream(response.getOutputStream());
        httpOutputStream.write(response, responseBody);
    }

}
