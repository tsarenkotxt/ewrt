package com.griddynamics.httpserver.example;

import com.griddynamics.httpserver.HttpServer;

public class Application {

    public static void main(String[] args) {
        HttpServer httpServer = new HttpServer.Builder().build();
        httpServer.run();
    }

}
