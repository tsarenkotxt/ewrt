package com.griddynamics.httpserver.api;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ResponseConverter {

    private ResponseConverter() {
    }

    private final static String SOCKET_NEW_LINE_CHARACTERS = "\r\n";
    private final static String GENERAL_HEADER_INDENTATION = " ";
    private final static String HEADER_JOIN_LINES_DELIMITER = "";
    private final static String DEFAULT_STATUS_MESSAGE = "OK";
    private final static String HTTP_PROTOCOL = "HTTP/1.1";
    private final static String HEADER_COLON = ": ";

    public static byte[] convertToByteArray(Map<String, String> header, int statusCode) {
        Optional<String> statusMessage = Optional.of(HttpStatus.statusCodeMessageMap.get(statusCode));
        String generalHeader = HTTP_PROTOCOL + GENERAL_HEADER_INDENTATION + statusCode +
                GENERAL_HEADER_INDENTATION + statusMessage.orElse(DEFAULT_STATUS_MESSAGE)
                + SOCKET_NEW_LINE_CHARACTERS;
        List<String> headersList = header.entrySet().stream()
                .map(e -> e.getKey() + HEADER_COLON + e.getValue() +
                        SOCKET_NEW_LINE_CHARACTERS).collect(Collectors.toList());
        String headersLine = String.join(HEADER_JOIN_LINES_DELIMITER, headersList)
                + SOCKET_NEW_LINE_CHARACTERS;
        String responseHeaders = generalHeader + headersLine;
        return responseHeaders.getBytes();
    }

}
