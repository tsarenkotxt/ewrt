package com.griddynamics.httpserver.api;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Response {

    private static final String CONTENT_TYPE_KEY = "Content-Type";
    private static final String CONTENT_LENGTH_KEY = "Content-Length";

    private int statusCode;
    private Map<String, String> header = new HashMap<>();
    private OutputStream outputStream;

    public void setContentType(String contentType) {
        header.put(CONTENT_TYPE_KEY, contentType);
    }

    public void setContentLength(int contentLength) {
        header.put(CONTENT_LENGTH_KEY, String.valueOf(contentLength));
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public byte[] headerGetBytes() {
        return ResponseConverter.convertToByteArray(header, statusCode);
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response = (Response) o;
        return statusCode == response.statusCode &&
                Objects.equals(header, response.header) &&
                Objects.equals(outputStream, response.outputStream);
    }

    @Override
    public int hashCode() {
        return Objects.hash(statusCode, header, outputStream);
    }

}
