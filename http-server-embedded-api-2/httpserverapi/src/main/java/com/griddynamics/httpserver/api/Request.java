package com.griddynamics.httpserver.api;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Request {

    private static final String CONTENT_TYPE_KEY = "Content-Type";
    private static final String CONTENT_LENGTH_KEY = "Content-Length";

    private String uri;
    private String method;
    private Map<String, String> parameters = new HashMap<>();
    private Map<String, String> header = new HashMap<>();
    private InputStream bodyInputStream;

    public String getContentType() {
        return header.get(CONTENT_TYPE_KEY);
    }

    public String getContentLength() {
        return header.get(CONTENT_LENGTH_KEY);
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public InputStream getBodyInputStream() {
        return bodyInputStream;
    }

    public void setBodyInputStream(InputStream bodyInputStream) {
        this.bodyInputStream = bodyInputStream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request that = (Request) o;
        return Objects.equals(uri, that.uri) &&
                Objects.equals(method, that.method) &&
                Objects.equals(parameters, that.parameters) &&
                Objects.equals(header, that.header) &&
                Objects.equals(bodyInputStream, that.bodyInputStream);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uri, method, parameters, header, bodyInputStream);
    }

}