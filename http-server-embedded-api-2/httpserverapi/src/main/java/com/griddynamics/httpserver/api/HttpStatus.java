package com.griddynamics.httpserver.api;

import java.util.HashMap;
import java.util.Map;

public final class HttpStatus {

    private HttpStatus() {
    }

    public static final Map<Integer, String> statusCodeMessageMap = new HashMap<>();

    static {
        statusCodeMessageMap.put(100, "Continue");
        statusCodeMessageMap.put(101, "Switching Protocols");
        statusCodeMessageMap.put(102, "Processing");
        statusCodeMessageMap.put(200, "OK");
        statusCodeMessageMap.put(201, "Created");
        statusCodeMessageMap.put(202, "Accepted");
        statusCodeMessageMap.put(203, "Non-Authoritative Information");
        statusCodeMessageMap.put(204, "No Content");
        statusCodeMessageMap.put(205, "Reset Content");
        statusCodeMessageMap.put(206, "Partial Content ");
        statusCodeMessageMap.put(207, "Multi-Status");
        statusCodeMessageMap.put(208, "Already Reported ");
        statusCodeMessageMap.put(226, "IM Used");
        statusCodeMessageMap.put(300, "Multiple Choices");
        statusCodeMessageMap.put(301, "Moved Permanently");
        statusCodeMessageMap.put(302, "Found");
        statusCodeMessageMap.put(303, "See Other");
        statusCodeMessageMap.put(304, "Not Modified");
        statusCodeMessageMap.put(305, "Use Proxy");
        statusCodeMessageMap.put(306, "Switch Proxy");
        statusCodeMessageMap.put(307, "Temporary Redirect");
        statusCodeMessageMap.put(308, "Permanent Redirect");
        statusCodeMessageMap.put(400, "Bad Request");
        statusCodeMessageMap.put(401, "Unauthorized");
        statusCodeMessageMap.put(402, "Payment Required");
        statusCodeMessageMap.put(403, "Forbidden");
        statusCodeMessageMap.put(404, "Not Found");
        statusCodeMessageMap.put(405, "Method Not Allowed");
        statusCodeMessageMap.put(406, "Not Acceptable");
        statusCodeMessageMap.put(407, "Proxy Authentication Required");
        statusCodeMessageMap.put(408, "Request Timeout");
        statusCodeMessageMap.put(409, "Conflict");
        statusCodeMessageMap.put(410, "Gone");
        statusCodeMessageMap.put(411, "Length Required");
        statusCodeMessageMap.put(412, "Precondition Failed");
        statusCodeMessageMap.put(413, "Payload Too Large");
        statusCodeMessageMap.put(414, "URI Too Long");
        statusCodeMessageMap.put(415, "Unsupported Media Type");
        statusCodeMessageMap.put(416, "Range Not Satisfiable");
        statusCodeMessageMap.put(417, "Expectation Failed");
        statusCodeMessageMap.put(418, "I'm a teapot");
        statusCodeMessageMap.put(421, "Misdirected Request");
        statusCodeMessageMap.put(422, "Unprocessable Entity");
        statusCodeMessageMap.put(423, "Locked");
        statusCodeMessageMap.put(424, "Failed Dependency");
        statusCodeMessageMap.put(426, "Upgrade Required");
        statusCodeMessageMap.put(428, "Precondition Required");
        statusCodeMessageMap.put(429, "Too Many Requests");
        statusCodeMessageMap.put(431, "Request Header Fields Too Large");
        statusCodeMessageMap.put(451, "Unavailable For Legal Reasons");
        statusCodeMessageMap.put(500, "Internal Server Error");
        statusCodeMessageMap.put(501, "Not Implemented");
        statusCodeMessageMap.put(502, "Bad Gateway");
        statusCodeMessageMap.put(503, "Service Unavailable");
        statusCodeMessageMap.put(504, "Gateway Timeout");
        statusCodeMessageMap.put(505, "HTTP Version Not Supported");
        statusCodeMessageMap.put(506, "Variant Also Negotiates");
        statusCodeMessageMap.put(507, "Insufficient Storage");
        statusCodeMessageMap.put(508, "Loop Detected");
        statusCodeMessageMap.put(510, "Not Extended");
        statusCodeMessageMap.put(511, "Network Authentication Required");
    }

}
