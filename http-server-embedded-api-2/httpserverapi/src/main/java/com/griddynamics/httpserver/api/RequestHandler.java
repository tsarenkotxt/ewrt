package com.griddynamics.httpserver.api;

import java.io.BufferedOutputStream;

public abstract class RequestHandler {

    abstract public String requestMapping();

    public void doGet(Request request, Response response) throws Exception {
        String json = "\"No handler found for GET mapping: " + request.getUri() + "\"";
        byte[] body = json.getBytes();
        response.setContentType("application/json");
        response.setContentLength(body.length);
        response.setStatusCode(404);
        byte[] header = response.headerGetBytes();
        BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
        outputStream.write(header, 0, header.length);
        outputStream.write(body, 0, body.length);
        outputStream.flush();
        outputStream.close();
    }

    public void doPost(Request request, Response response) throws Exception {
        String json = "\"No handler found for POST mapping: " + request.getUri() + "\"";
        byte[] body = json.getBytes();
        response.setContentType("application/json");
        response.setContentLength(body.length);
        response.setStatusCode(404);
        byte[] header = response.headerGetBytes();
        BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
        outputStream.write(header, 0, header.length);
        outputStream.write(body, 0, body.length);
        outputStream.flush();
        outputStream.close();
    }

    public void doPut(Request request, Response response) throws Exception {
        String json = "\"No handler found for PUT mapping: " + request.getUri() + "\"";
        byte[] body = json.getBytes();
        response.setContentType("application/json");
        response.setContentLength(body.length);
        response.setStatusCode(404);
        byte[] header = response.headerGetBytes();
        BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
        outputStream.write(header, 0, header.length);
        outputStream.write(body, 0, body.length);
        outputStream.flush();
        outputStream.close();
    }

    public void doDelete(Request request, Response response) throws Exception {
        String json = "\"No handler found for DELETE mapping: " + request.getUri() + "\"";
        byte[] body = json.getBytes();
        response.setContentType("application/json");
        response.setContentLength(body.length);
        response.setStatusCode(404);
        byte[] header = response.headerGetBytes();
        BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
        outputStream.write(header, 0, header.length);
        outputStream.write(body, 0, body.length);
        outputStream.flush();
        outputStream.close();
    }

}
