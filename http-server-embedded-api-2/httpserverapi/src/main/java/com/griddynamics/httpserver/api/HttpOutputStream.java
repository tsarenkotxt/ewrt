package com.griddynamics.httpserver.api;

import java.io.IOException;
import java.io.OutputStream;

public class HttpOutputStream {

    private final int MAX_BODY_SIZE = 2048;

    private final OutputStream output;

    public HttpOutputStream(OutputStream output) {
        this.output = output;
    }

    public void write(Response response, byte[] responseBody) throws IOException {
        if (responseBody.length > MAX_BODY_SIZE) {
            throw new IllegalArgumentException("Body size cannot exceed 2048 byte");
        }
        response.getHeader().put("Connection", "Close");
        response.setContentLength(responseBody.length);
        output.write(response.headerGetBytes());
        output.write(responseBody);
        output.flush();
    }

}
